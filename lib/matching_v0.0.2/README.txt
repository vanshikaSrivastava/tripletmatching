=================================================================================================================
       Installation and Usage Guide for CUDA based Geometry-aware Feature Matcher
copyright 2016 Vanshika Srivastava and Rajvi Shah (vanshika.srivastava.iiith@gmail.com and rajvi.a.shah@gmail.com)
-----------------------------------------------------------------------------------------------------------------

This source package implements a parallel programming system for fast and accurate geometry-aware 
matching of SIFT-like features as described in the following paper,

1)  Geometry-aware Feature Matching for Structure from Motion Application,
    Rajvi Shah, Vanshika Srivastava, P J Narayanan. [WACV 2015]

Application of geometry-aware feature matching for a multistage structure from  
motion framework is described in the following paper,

2)  Multistage SFM : Revisiting Incremental Structure from Motion. 
    Rajvi Shah, Aditya Deshpande, P J Narayanan [3DV 2014]

For more information and latest version of the code visit,
http://cvit.iiit.ac.in/projects/multistagesfm/

THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED, 
IMPLIED OR OTHERWISE.`

You can use, copy, modify and distribute this software and its documentation 
for non-commercial purpose without fee, provided that the copyright notices 
appear in all copies of it. If you do so, please cite our WACV'15 paper.  

You can write to Vanshika Srivastava (vanshika.srivastava.iiith@gmail.com) for reporting bugs or for  
doubts/clarification. 

===============================================================================
**CAUTION** : Where is this matching expected to work?
-------------------------------------------------------------------------------
This matching code leverages epipolar geometry between two images for fast and 
accurate feature matching. This makes its application suitable for SFM and 
stereo vision. It will not produce matches if the geometry between the two 
images cannot be captured by epipolar geometry (more precisely a fundamental 
matrix), for example images of a planar scene, or captured by a camera rotating 
around a fixed point (images related by a single Homography).

===============================================================================
I. Source files in this package
-------------------------------------------------------------------------------
The following file provide example code for using the matcher code for simple 
pair-wise matching or match-graph construction from multiple images,
|-- match_image_pair.cpp


This following source files implements the core functionality of this package,
|-- defs.h
    Common definitions and includes
|-- Matcher.h, Matcher.cpp  
    Class with various matching, and other utility functions. 
|-- Gridder.h, Gridder.cpp
    Class responsible for dividing image features into bins. 
    Stores maps from feature to cell and vice versa.

===============================================================================
II. About the third party libraries, binaries and helper scripts
-------------------------------------------------------------------------------
OpenCV
------
Our code uses OpenCV for fundamental matrix computation. If you already have 
opencv version 2.4 or higher installed, please ensure that pkg-config can find 
the correct paths to its lib and include files. Use the following commands to 
test if the opencv include paths and libs are properly displayed.

	> pkg-config --cflags opencv
	> pkg-config --libs opencv

For fresh installation of opencv, please follow,
(Ubuntu)  https://help.ubuntu.com/community/OpenCV
(General) http://docs.opencv.org/trunk/doc/tutorials/introduction/linux_install/linux_install.html  

SiftGPU
-------
For initial matching and sift extraction.
Present in the lib folder of our code.

CUDA
----
CUDA is a parallel computing platform and application programming interface (API) model created by NVIDIA. 
To run multiple threads for the program, a CUDA enabled GPU should be available.

Thrust
------
Thrust is a C++ template library for CUDA based on the Standard Template Library (STL). Thrust allows you to implement high performance parallel applications with minimal programming effort through a high-level interface that is fully interoperable with CUDA C.
Installing the CUDA Toolkit will copy Thrust header files to the standard CUDA include directory for your system. Since Thrust is a template library of header files, no further installation is necessary to start using Thrust.

http://docs.nvidia.com/cuda/thrust/index.html

===============================================================================
III. How to compile our code?
-------------------------------------------------------------------------------
The source code is written and tested under 64-bit Linux environment. Please 
follow the step-by-step instructions given below to successfully compile it,

1.  Make sure, all the dependecies mentioned above are correctly installed .

2.  > cd src
    > make

    This creates one binary in bin directory called match_pairs

===============================================================================
IV. How to independently use the binary for creating match-graph
-------------------------------------------------------------------------------
bin/match_pairs [list of image names] [corresponding list of keys] [bundler output] [output file]

-------
Example
-------
Example is provided in the scripts folder

> sh sampleRunFullMatching.sh
../bin/match_pairs ../data/dataset/all_img.txt ../data/dataset/all_key.txt ../data/pairs-0.txt myfile.txt

===============================================================================
V. How to independently use the binary for pairwise matching
-------------------------------------------------------------------------------

bin/match_pairs [file specifying images data]

-------
Example
-------
Example is provided in the scripts folder

> sh sampleRunPairWise.sh
../bin/match_pairs ../data/sampleInputPairWise.txt

===============================================================================
For Questions/Suggestions/Help contact us
-------------------------------------------------------------------------------

Vanshika Srivastava (vanshika.srivastava.iiith@gmail.com)
Rajvi Shah (rajvi.a.shah@gmail.com)
P J Narayanan (pjn@iiit.ac.in)
