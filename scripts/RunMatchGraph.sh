../bin/tripletMatching ../data/all_img.txt ../data/all_key.txt ../data/dims.txt  > logFile.log

file_name=logFile.log
 
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
 
new_fileName=$file_name.$current_time
 
mv $file_name ../results/$new_fileName
