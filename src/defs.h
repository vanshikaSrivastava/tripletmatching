#include <stdio.h>
#include <stdlib.h>


#include <cmath>
#include <time.h>

#include <string>
#include <stack>
#include <vector>
#include <map>
#include <unordered_map>


#include <fstream>
#include <iostream>
#include <jpeglib.h>


#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv/cvaux.h>
#include <opencv/cxcore.h>


#include "../lib/SiftGPU/src/SiftGPU/SiftGPU.h"



using namespace std;
