#include "defs.h"

int readSift(char *path,vector<SiftGPU::SiftKeypoint> &keys,vector<float > &descriptors)
{

	int numSift = 0, descSize = 0;
	
	FILE *img = fopen(path,"r");

	fscanf(img,"%d",&numSift);
	fscanf(img,"%d",&descSize);


	keys.resize(numSift);    
	descriptors.resize(128*numSift);

	for(int i = 0; i < numSift; i++)
	{   
		fscanf(img,"%f%f%f%f",&keys[i].y,&keys[i].x,&keys[i].s,&keys[i].o);
		for(int j = 0; j < 128; j++)
		{
			fscanf(img,"%f",&descriptors[(i*128)+j]);
			descriptors[(i*128)+j] = (descriptors[(i*128)+j])/512;
		}

	}   
	fclose(img);

	return numSift;

}

int readMatches(char *path,vector<int> &matches)
{
	FILE *fp = fopen(path,"r");

	int numMatches;
	fscanf(fp,"%d",&numMatches);

	for(int k = 0 ; k < numMatches; k++)
	{
		int i,j;
		fscanf(fp,"%d %d",&i,&j);
		matches[i] = j;
	}
	fclose(fp);
	return numMatches;
}
void writeMatches(int n, vector<int> &matches,vector<SiftGPU::SiftKeypoint> &keys1,vector<SiftGPU::SiftKeypoint> &keys2)
{
	FILE *fp = fopen("12_xyz.txt","w");
	for(int i = 0 ; i < matches.size(); i++)
	{
		if(matches[i]!=-1)
		{
			fprintf(fp,"%f %f %f\n",keys1[i].x,keys1[i].y,1.0);
			fprintf(fp,"%f %f %f\n",keys2[matches[i]].x,keys2[matches[i]].y,1.0);
		}
	}
	fclose(fp);
}
int main()
{

	int num_keys;

	vector<SiftGPU::SiftKeypoint> keys1;
	vector<SiftGPU::SiftKeypoint> keys2;
	vector<SiftGPU::SiftKeypoint> keys3;
	vector<float> descriptors1;
	vector<float> descriptors2;
	vector<float> descriptors3;

	int num1 = readSift((char *)"../data/pair/1.key",keys1,descriptors1);
	int num2 = readSift((char *)"../data/pair/2.key",keys2,descriptors2);
	int num3 = readSift((char *)"../data/pair/4.key",keys3,descriptors3);

	cout << "Sift reading complete\n" << num1 << " " << num2 << " " << num3 << endl ;

	vector<int> matches12(num1,-1);
	vector<int> matches23(num2,-1);

	int n1 = readMatches((char *)"1_2.txt",matches12);
	int n2 = readMatches((char *)"2_3.txt",matches23);
	
	cout << "Matches reading complete\n" << n1 << " " << n2 <<  endl ;

	//writeMatches(n1, matches12,keys1,keys2);

	FILE *fp1 = fopen((char *)"1.txt","w");
	FILE *fp2 = fopen((char *)"2.txt","w");
	FILE *fp3 = fopen((char *)"3.txt","w");

	for(int i = 0; i < num1; i++)
	{
		if(matches12[i] != -1 && matches23[matches12[i]] != -1)
		{
			fprintf(fp1,"%f %f %f\n",keys1[i].x,keys1[i].y,1.0);
			fprintf(fp2,"%f %f %f\n",keys2[matches12[i]].x,keys2[matches12[i]].y,1.0);
			fprintf(fp3,"%f %f %f\n",keys3[matches23[matches12[i]]].x,keys3[matches23[matches12[i]]].y,1.0);
		}

	}
	fclose(fp1);
	fclose(fp2);
	fclose(fp3);
	return 0;
}
