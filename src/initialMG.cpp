#include "defs.h"
#include "sifts.h"

#include "../lib/matching_v0.0.2/src/defs.h"
#include "../lib/matching_v0.0.2/src/Matcher.h"
#include "../lib/matching_v0.0.2/src/keys2a.h"
#include "../lib/matching_v0.0.2/src/Gridder.h"
#include "../lib/matching_v0.0.2/src/Geometric.h"
#include "../lib/matching_v0.0.2/src/argvparser.h"

#include <sys/time.h>

using namespace std;
using namespace cv;
using namespace match;
using namespace CommandLineProcessing;


int initializeMatchGraph(int n,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph)
{
	for(int i = 0 ; i < n; i++)
	{
		matchGraph[i].resize(n);
		for(int j = 0 ; j < n; j++)
		{
			matchGraph[i][j].first = 0;
			matchGraph[i][j].second.resize(0);
		}
	}
}


/*
int constructInitialMatchGraph(vector<vector<SiftGPU::SiftKeypoint> > &allKeys,vector<vector<float> > &allDescriptors,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph)
{
	int n = allKeys.size();
	for(int i = 0 ; i < n; i++)
	{
		matchGraph[i].resize(n);
		for(int j = 0 ; j <= i; j++)
		{
			matchGraph[i][j].first = 0;
			matchGraph[i][j].second.resize(0);
		}
		for(int j = i+1 ; j < n ; j++)
		{
			cout << i << " " << j << endl ;
			match20pc(allKeys[i],allKeys[j],allDescriptors[i],allDescriptors[j],matchGraph[i][j]);
		}
	}


	return 0;
}
*/

void writeMatchesToFile(int n1,int n2,FeatureMatcher &matcher, const char* fileName,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph) {

  FILE* fn = fopen(fileName,"a");
  if(fn == NULL) {
    printf("\nCould not open file to write");
  }
   fprintf(fn,"%d %d\n", n1,n2);
   fprintf(fn,"%d\n", matcher.matches.size());
   matchGraph[n1][n2].first = matcher.matches.size();
   matchGraph[n1][n2].second.resize(matcher.matches.size());
  for(int i=0; i < matcher.matches.size(); i++) {
	  matchGraph[n1][n2].second[i].first = matcher.matches[i].first;
	  matchGraph[n1][n2].second[i].second = matcher.matches[i].second;
    fprintf(fn,"%d %d\n", matcher.matches[i].first, matcher.matches[i].second);
  }
  fclose(fn); 
}


void writetMatchesToFile(int n1,int n2,vector<pair<int,int> > &matches, const char* fileName,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph) {

  FILE* fn = fopen(fileName,"a");
  if(fn == NULL) {
    printf("\nCould not open file to write");
  }
   fprintf(fn,"%d %d\n", n1,n2);
   fprintf(fn,"%d\n", matches.size());
   matchGraph[n1][n2].first = matches.size();
   matchGraph[n1][n2].second.resize(matches.size());
  for(int i=0; i < matches.size(); i++) {
	  matchGraph[n1][n2].second[i].first = matches[i].first;
	  matchGraph[n1][n2].second[i].second = matches[i].second;
    fprintf(fn,"%d %d\n", matches[i].first, matches[i].second);
  }
  fclose(fn); 
}




bool matchImagePair20(int i,int j,string imagePath1,string imagePath2,int qW,int qH,int rW,int rH,int nPts1,int nPts2,keypt_t* queryKeyInfo,keypt_t* refKeyInfo,unsigned char* queryKey,unsigned char* refKey,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph)
{

//	cout << imagePath1 << endl << imagePath2 << endl << qW << " " << qH << " " << rW << " " << rH << " " << endl << nPts1 << " " << nPts2 << endl << queryKeyInfo << " " << refKeyInfo << endl << queryKey << " " << refKey << endl ; 

  bool visualize = false ; 
//  bool save_visualization = cmd.foundOption("save_visualization");

  
  /// Initialize Gridder
  
  vector< vector<double> > refRectEdges;
  vector< vector<double> > srcRectEdges;

  /// Precompute Four Edges of Image Rectangles
  geometry::ComputeRectangleEdges((double)rW, (double)rH, refRectEdges);
  geometry::ComputeRectangleEdges((double)qW, (double)qH, srcRectEdges);

  /// Initialize Matcher
  match::FeatureMatcher matcher;
 // matcher.queryImage = imread(imagePath1.c_str(),
 //     CV_LOAD_IMAGE_COLOR);

 // matcher.referenceImage = imread(imagePath2.c_str(),
  //    CV_LOAD_IMAGE_COLOR);

  matcher.setNumSrcPoints( nPts1 );
  matcher.setSrcKeys(queryKeyInfo, queryKey);

  matcher.setImageDims(qW, qH, rW, rH);

  matcher.setNumRefPoints( nPts2 );
  matcher.setRefKeys(refKeyInfo, refKey);

  matcher.setSrcRectEdges(srcRectEdges);
  matcher.setRefRectEdges(refRectEdges);

  /// Find F estimate using 20% top features
  /// First perform global Kd-tree based matching
 
  matcher.globalMatch(20, true);
  if(matcher.matches.size() < 16) {
    printf("\nCould Not Find sufficient matches, Exiting\n");
    return false;
  }


  printf("\nFound %d matches between %d %d points\n", matcher.matches.size(), nPts1, nPts2);

  string str1 = "../results/init.matchGraph.txt";
  writeMatchesToFile(i,j,matcher, str1.c_str(),matchGraph);
  fflush(stdout);

  if(visualize) {
 //   if(save_visualization) {
 //     string str = result_path + "matches.jpg";
 //     matcher.visualizeMatches(str.c_str());
 //   } else {
      matcher.visualizeMatches(NULL); 
 //   }
  }

  return true;
}




int main(int argc,char *argv[])
{
	if(argc != 4)
	{
		printf("To run the program, type ./a.out intput_images_list keys_list dims_list\n");
		return 0;
	}

	struct timespec t1, t2, t3, t4, t5;



	// Read all image names and corresponding key file names
	std::vector<std::string> listImg;
	std::ifstream imgFiles;
	std::string line;
	imgFiles.open(argv[1]);

	while(!imgFiles.eof())
	{
		imgFiles >> line;
		listImg.push_back(line);
	}

	imgFiles.close();

	std::vector<std::string> listKey;
	std::ifstream keyFiles;
	keyFiles.open(argv[2]);

	while(!keyFiles.eof())
	{
		keyFiles >> line;
		listKey.push_back(line);
	}

	keyFiles.close();

	listKey.resize(listKey.size()-1);
	listImg.resize(listImg.size()-1);
	// Load all the features into the main memory

	vector<pair<int,int> > dims;
	std::ifstream dimsFiles;
	int h,w;
	dimsFiles.open(argv[3]);

	while(!dimsFiles.eof())
	{
		dimsFiles >> w >> h;
		dims.push_back(make_pair(w,h));
	}

	dimsFiles.close();

	std::vector<unsigned char*> allDescriptors(listKey.size());
	std::vector<keypt_t* > allKeys(listKey.size());
	vector<int> numSift(listKey.size(),0);


	std::cout << listKey.size() << std::endl;

	for(int i = 0 ; i < listKey.size(); i++)
	{
		std::cout << listKey[i] << "\n" ; 
		numSift[i] = ReadKeyFile((char *)listKey[i].c_str(),&allDescriptors[i], &allKeys[i]);

		printf("reading id %d\n",i);
	}

	cout << "All sift reading complete\n" ;

	vector<vector<pair<int,vector<pair<int,int> > > > > matchGraph(listKey.size(), vector<pair<int,vector<pair<int,int> > > > (1));
	
	cout << "Initializing match graph...\n" ;

	clock_gettime(CLOCK_MONOTONIC, &t1);
	
	initializeMatchGraph(allKeys.size(),matchGraph);


	cout << "MatchGraph initialization complete\n" ;
	cout << "Computing initial matchgraph...\n" ;


	
	int n = allKeys.size();

	clock_gettime(CLOCK_MONOTONIC, &t1);
	FILE *mf = fopen("../results/init.matchGraph.txt","w");
	fclose(mf);

	for(int i = 0 ; i < n; i++)
	{
		for(int j = i+1 ; j < n; j++)
		{
			cout << "Full Matching between " << i << " " << j << endl;
			bool status;
			status = matchImagePair20(i,j,listImg[i],listImg[j],dims[i].first,dims[i].second,dims[j].first,dims[j].second,numSift[i],numSift[j],allKeys[i],allKeys[j],allDescriptors[i],allDescriptors[j],matchGraph);
		}
	}

	clock_gettime(CLOCK_MONOTONIC, &t2);
	double time1 = ((t2.tv_sec - t1.tv_sec)*1000) + (((double)(t2.tv_nsec - t1.tv_nsec))/1000000.0);
	
	cout << "Match Graph construction complete\n" ;
	cout << "Time taken : " << time1 << endl;

	return 0;
}
