#include "defs.h"
#include "sifts.h"

#include "../lib/matching_v0.0.2/src/defs.h"
#include "../lib/matching_v0.0.2/src/Matcher.h"
#include "../lib/matching_v0.0.2/src/keys2a.h"
#include "../lib/matching_v0.0.2/src/Gridder.h"
#include "../lib/matching_v0.0.2/src/Geometric.h"
#include "../lib/matching_v0.0.2/src/argvparser.h"

#include <sys/time.h>

using namespace std;
using namespace cv;
using namespace match;
using namespace CommandLineProcessing;

void writeMatchesToMatchGraph(int n1,int n2,FeatureMatcher &matcher,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph) {

   matchGraph[n1][n2].first = matcher.matches.size();
   matchGraph[n1][n2].second.resize(matcher.matches.size());
  for(int i=0; i < matcher.matches.size(); i++) {
	  matchGraph[n1][n2].second[i].first = matcher.matches[i].first;
	  matchGraph[n1][n2].second[i].second = matcher.matches[i].second;
  }
}


bool matchImagePair20(int i,int j,string imagePath1,string imagePath2,int qW,int qH,int rW,int rH,int nPts1,int nPts2,keypt_t* queryKeyInfo,keypt_t* refKeyInfo,unsigned char* queryKey,unsigned char* refKey,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph)
{


  bool visualize = false ; 
//  bool save_visualization = cmd.foundOption("save_visualization");

  
  /// Initialize Gridder
  
  vector< vector<double> > refRectEdges;
  vector< vector<double> > srcRectEdges;

  /// Precompute Four Edges of Image Rectangles
  geometry::ComputeRectangleEdges((double)rW, (double)rH, refRectEdges);
  geometry::ComputeRectangleEdges((double)qW, (double)qH, srcRectEdges);

  /// Initialize Matcher
  match::FeatureMatcher matcher;
 // matcher.queryImage = imread(imagePath1.c_str(),
 //     CV_LOAD_IMAGE_COLOR);

 // matcher.referenceImage = imread(imagePath2.c_str(),
  //    CV_LOAD_IMAGE_COLOR);

  matcher.setNumSrcPoints( nPts1 );
  matcher.setSrcKeys(queryKeyInfo, queryKey);

  matcher.setImageDims(qW, qH, rW, rH);

  matcher.setNumRefPoints( nPts2 );
  matcher.setRefKeys(refKeyInfo, refKey);

  matcher.setSrcRectEdges(srcRectEdges);
  matcher.setRefRectEdges(refRectEdges);

  /// Find F estimate using 20% top features
  /// First perform global Kd-tree based matching
 
  matcher.globalMatch(20, true);
  if(matcher.matches.size() < 16) {
    printf("\nCould Not Find sufficient matches, Exiting\n");
    return false;
  }


  printf("\nFound %d matches between %d %d points\n", matcher.matches.size(), nPts1, nPts2);

  string str1 = "../results/init.matchGraph.txt";
  writeMatchesToMatchGraph(i,j,matcher,matchGraph);
  fflush(stdout);

  if(visualize) {
 //   if(save_visualization) {
 //     string str = result_path + "matches.jpg";
 //     matcher.visualizeMatches(str.c_str());
 //   } else {
      matcher.visualizeMatches(NULL); 
 //   }
  }

  return true;
}


int initializeMatchGraph(int n,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph)
{
	for(int i = 0 ; i < n; i++)
	{
		matchGraph[i].resize(n);
		for(int j = 0 ; j < n; j++)
		{
			matchGraph[i][j].first = 0;
			matchGraph[i][j].second.resize(0);
		}
	}
}

int readInitialMatchGraph(char *matchGraphFile, vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph)
{
	
	std::ifstream matchFile;
	matchFile.open(matchGraphFile);

	while(!matchFile.eof())
	{
		
		int i,j;
		matchFile >> i >> j;

		cout << "Reading match pair for id : " << i << " " << j << endl;

		int numMatches;

		matchFile >> numMatches ;

		matchGraph[i][j].first = numMatches;
		matchGraph[i][j].second.resize(numMatches);

		for(int c = 0 ; c <  numMatches; c++)
		{
			int n1,n2;
			matchFile >> n1 >> n2 ;
			matchGraph[i][j].second[c].first= n1;
			matchGraph[i][j].second[c].second= n2;

		}
	}

	matchFile.close();

	return 0;
}


int timeval_subtract(struct timeval *result, 
        struct timeval *t2, 
        struct timeval *t1) {
    long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;

    return (diff<0);
}

void writeMatchesToFile(int n1,int n2,FeatureMatcher &matcher, const char* fileName,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph) {

  FILE* fn = fopen(fileName,"a");
  if(fn == NULL) {
    printf("\nCould not open file to write");
  }
   fprintf(fn,"%d %d\n", n1,n2);
   fprintf(fn,"%d\n", matcher.matches.size());
   matchGraph[n1][n2].first = matcher.matches.size();
   matchGraph[n1][n2].second.resize(matcher.matches.size());
  for(int i=0; i < matcher.matches.size(); i++) {
	  matchGraph[n1][n2].second[i].first = matcher.matches[i].first;
	  matchGraph[n1][n2].second[i].second = matcher.matches[i].second;
    fprintf(fn,"%d %d\n", matcher.matches[i].first, matcher.matches[i].second);
  }
  fclose(fn); 
}


void writetMatchesToFile(int n1,int n2,vector<pair<int,int> > &matches, const char* fileName,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph) {

  FILE* fn = fopen(fileName,"a");
  if(fn == NULL) {
    printf("\nCould not open file to write");
  }
   fprintf(fn,"%d %d\n", n1,n2);
   fprintf(fn,"%d\n", matches.size());
   matchGraph[n1][n2].first = matches.size();
   matchGraph[n1][n2].second.resize(matches.size());
  for(int i=0; i < matches.size(); i++) {
	  matchGraph[n1][n2].second[i].first = matches[i].first;
	  matchGraph[n1][n2].second[i].second = matches[i].second;
    fprintf(fn,"%d %d\n", matches[i].first, matches[i].second);
  }
  fclose(fn); 
}




bool matchImagePair(int i,int j,string imagePath1,string imagePath2,int qW,int qH,int rW,int rH,int nPts1,int nPts2,keypt_t* queryKeyInfo,keypt_t* refKeyInfo,unsigned char* queryKey,unsigned char* refKey,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph)
{

//	cout << imagePath1 << endl << imagePath2 << endl << qW << " " << qH << " " << rW << " " << rH << " " << endl << nPts1 << " " << nPts2 << endl << queryKeyInfo << " " << refKeyInfo << endl << queryKey << " " << refKey << endl ; 

  bool visualize = false ; 
//  bool save_visualization = cmd.foundOption("save_visualization");

  
  struct timeval t1, t2, t3;
  gettimeofday(&t1, NULL);
  /// Initialize Gridder
  Gridder srcGrid(16, qW, qH, nPts1, queryKeyInfo);
  Gridder refGrid(16, rW, rH, nPts2, refKeyInfo);
  
  vector< vector<double> > refRectEdges;
  vector< vector<double> > srcRectEdges;

  /// Precompute Four Edges of Image Rectangles
  geometry::ComputeRectangleEdges((double)rW, (double)rH, refRectEdges);
  geometry::ComputeRectangleEdges((double)qW, (double)qH, srcRectEdges);

  /// Initialize Matcher
  match::FeatureMatcher matcher;
 // matcher.queryImage = imread(imagePath1.c_str(),
 //     CV_LOAD_IMAGE_COLOR);

 // matcher.referenceImage = imread(imagePath2.c_str(),
  //    CV_LOAD_IMAGE_COLOR);

  matcher.setNumSrcPoints( nPts1 );
  matcher.setSrcKeys(queryKeyInfo, queryKey);

  matcher.setImageDims(qW, qH, rW, rH);

  matcher.setNumRefPoints( nPts2 );
  matcher.setRefKeys(refKeyInfo, refKey);

  matcher.setSrcRectEdges(srcRectEdges);
  matcher.setRefRectEdges(refRectEdges);

  /// Find F estimate using 20% top features
  /// First perform global Kd-tree based matching
 
  matcher.globalMatch(20, true);
  if(matcher.matches.size() < 16) {
    printf("\nCould Not Find sufficient matches, Exiting\n");
    return false;
  }

  vector<double> fMatrix(9);
  double* F_ptr = fMatrix.data();
  int inliers = matcher.computeFmatrix(F_ptr);
  if(inliers == -1) { 
    printf("\nCould Not Find 16 Inliers to computed F, Exiting\n");
    return false;
  }

  matcher.setQueryGrid(&srcGrid);
  matcher.setRefGrid(&refGrid);
  matcher.setFMatrix(fMatrix); 
  
  /// Actual Computation
  matcher.computeEpipolarLines();
  matcher.clusterPointsFast();

  int numMatches = matcher.match();
  gettimeofday(&t2, NULL);

  /// For two-way matching, initialize another instance of matcher
  /// Initialize with opposite source and target
  /// Pass transpose(F) as F matrix
  /// Find common matches between matcher and matcher1
  


  timeval_subtract(&t3,&t2,&t1);

  printf("\nTotal Time to Match : %ld.%06d", t3.tv_sec, t3.tv_usec);
  printf("\nFound %d matches between %d %d points\n", numMatches, nPts1, nPts2);

  string str1 = "../results/matchGraph.txt";
  writeMatchesToFile(i,j,matcher, str1.c_str(),matchGraph);
  fflush(stdout);

  if(visualize) {
 //   if(save_visualization) {
 //     string str = result_path + "matches.jpg";
 //     matcher.visualizeMatches(str.c_str());
 //   } else {
      matcher.visualizeMatches(NULL); 
 //   }
  }

  return true;
}

int computeFmatrix(double* Fdata,vector<pair<int,int> > &matches,keypt_t* srcKeysInfo, keypt_t* refKeysInfo) 
{
  vector<cv::Point2f> imgPts1, imgPts2;

    if (matches.size() == 0)
    {
  	cout << "Matches size : " << matches.size() << endl ;
    	return -1;
    }
  for(int i=0; i < matches.size(); i++) {
    int idx1 = matches[i].first;
    int idx2 = matches[i].second;

    imgPts1.push_back( cv::Point2f(srcKeysInfo[idx1].x,srcKeysInfo[idx1].y));
    imgPts2.push_back( cv::Point2f(refKeysInfo[idx2].x,refKeysInfo[idx2].y));

  }
  cv::Mat Finliers;

  /// Please see OpenCV documentation before you change parameters
  cv::Mat F = findFundamentalMat(imgPts1, imgPts2, CV_FM_RANSAC, 1, 0.99, Finliers);

  /// Delete all outlier matches
  vector<pair<int, int> > :: iterator itr = matches.begin();
  for (int i=0; i< Finliers.size().height; i++) {
    if (Finliers.at<char>(i) != 1) {
      matches.erase(itr);  
    }
    itr++;
  }
  /// Check if sufficient matches remian inliers
  //  This can be made stricter (See paper)

  int matchCount = (int)(matches.size());
  if(matchCount >= 16) {
    const double* ptr = F.ptr<double>();
    if(ptr[8] == 0)
	    return -1;
    for(int f=0; f < 9; f++) {
      Fdata[f] = ptr[f]/ptr[8];
    }

    return (int)matches.size();
  }
  cout << "Match count : " << matchCount << endl ; 

  return -1;
}

int maX(int a,int b,int c)
{
	return max(max(a,b),c);
}

void visualizeTMatches(char *im1,char *im2,int i,int j,keypt_t* srcKeysInfo, keypt_t* refKeysInfo, vector<pair<int,int> > &matches) 
{

	Mat queryImage = imread(im1);
	cv::Size sz = queryImage.size();
	int qWidth = sz.width;
	int qHeight = sz.height;

	Mat referenceImage = imread(im2);
	cv::Size sz1 = referenceImage.size();
	int rWidth = sz1.width;
	int rHeight = sz1.height;

	int canvasWidth = qWidth + rWidth;
	int canvasHeight = qHeight > rHeight ? qHeight : rHeight;
	cv::Mat canvas(canvasHeight, canvasWidth, CV_8UC3, cv::Scalar(0,0,0));

	cv::Rect roi1 = cv::Rect(0,0,qWidth,qHeight);
	cv::Mat canvas_roi1 = canvas(roi1);
	queryImage.copyTo(canvas_roi1);

	cv::Rect roi2 = cv::Rect(qWidth,0,rWidth,rHeight);
	cv::Mat canvas_roi2 = canvas(roi2);
	referenceImage.copyTo(canvas_roi2);

	for(int i=0; i < matches.size(); i++) {
		pair<int,int> p = matches[i];
		cv::Point pt1 = cv::Point(srcKeysInfo[p.first].x, srcKeysInfo[p.first].y);
		cv::Point pt2 = cv::Point(refKeysInfo[p.second].x + qWidth, refKeysInfo[p.second].y);

		cv::circle(canvas, pt1, 2, cv::Scalar(0,255,0), 5);
		cv::circle(canvas, pt2, 2, cv::Scalar(0,255,0), 5);
		cv::line(canvas, pt1, pt2, cv::Scalar(233,255,0), 4);

	}

	cv::namedWindow("TMatches",cv::WINDOW_NORMAL);
	imshow("TMatches", canvas);
	cv::waitKey();
}


void visualizeDBG(char *im1,char *im2,char *im3,vector<float> &l1,vector<float> &l2,float x1,float y1,float x2,float y2,float x3,float y3)
{
	Mat img1 = imread(im1);
	cv::Size sz = img1.size();
	int qWidth = sz.width;
	int qHeight = sz.height;

	Mat img2 = imread(im2);
	cv::Size sz1 = img2.size();
	int rWidth = sz1.width;
	int rHeight = sz1.height;

	Mat img3 = imread(im3);
	cv::Size sz2 = img3.size();
	int sWidth = sz2.width;
	int sHeight = sz2.height;


	int canvasWidth = qWidth + rWidth + sWidth;
	int canvasHeight = maX(qHeight,rHeight,sHeight);

	cv::Mat canvas(canvasHeight, canvasWidth, CV_8UC3, cv::Scalar(0,0,0));

	cv::Rect roi1 = cv::Rect(0,0,qWidth,qHeight);
	cv::Mat canvas_roi1 = canvas(roi1);
	img1.copyTo(canvas_roi1);

	cv::Rect roi2 = cv::Rect(qWidth,0,rWidth,rHeight);
	cv::Mat canvas_roi2 = canvas(roi2);
	img2.copyTo(canvas_roi2);

	cv::Rect roi3 = cv::Rect(qWidth+rWidth,0,sWidth,sHeight);
	cv::Mat canvas_roi3 = canvas(roi3);
	img3.copyTo(canvas_roi3);

	cv::Point pt1 = cv::Point(x1,y1);
	cv::Point pt2 = cv::Point(x2+qWidth,y2);
	cv::Point pt3 = cv::Point(x3+rWidth+qWidth,y3);

	cout << x1 << " " << y1 << " " << x2 << " " << y2 << " " << x3 << " " << y3 << endl ; 
	cout << qWidth << " " << qHeight << " " << rWidth << " " << rHeight << " " << sWidth << " " << sHeight << endl; 

	cv::circle(canvas, pt1, 2, cv::Scalar(0,255,0), 25);
	cv::circle(canvas, pt2, 2, cv::Scalar(0,255,0), 25);
	cv::circle(canvas, pt3, 2, cv::Scalar(0,255,0), 25);

//	cv::Point pt4 = cv::Point(qWidth+rWidth,(-1*(l1[2]-(l1[0]*sWidth/2.0)/l1[1])) + (sHeight/2.0));
//	cv::Point pt5 = cv::Point(qWidth+rWidth+sWidth,(-1*(l1[2]+(l1[0]*sWidth/2.0)/l1[1])) + (sHeight/2.0));

		cv::Point pt4 = cv::Point(qWidth+rWidth,-1*(l1[2])/l1[1]);
		cv::Point pt5 = cv::Point(qWidth+rWidth+sWidth,-1*(l1[2] + (l1[0]*sWidth))/l1[1]);

	//cv::Point pt6 = cv::Point(qWidth+rWidth,(-1*(l2[2]-(l2[0]*sWidth/2.0)/l2[1])) + (sHeight/2.0));
	//cv::Point pt7 = cv::Point(qWidth+rWidth+sWidth,(-1*(l2[2]+(l2[0]*sWidth/2.0)/l2[1])) + (sHeight/2.0));
	cv::Point pt6 = cv::Point(qWidth+rWidth,-1*l2[2]/l2[1]);
	cv::Point pt7 = cv::Point(qWidth+rWidth+sWidth,-1*(l2[2] + (l2[0]*sWidth))/l2[1]);

	cv::line(canvas, pt4, pt5, cv::Scalar(0,255,255), 5);
	cv::line(canvas, pt6, pt7, cv::Scalar(233,255,0), 5);

	cv::namedWindow("TripletTransfer",cv::WINDOW_NORMAL);
	imshow("TripletTransfer", canvas);
	cv::waitKey();
}





bool tripletTranfer(int i,int j,int k,vector<string> &imagePath,vector<pair<int,int> > &dim,vector<int> &numSift,vector<keypt_t*> &KeyInfo,vector<unsigned char*> &desc,vector<vector<pair<int,vector<pair<int,int> > > > > &matchGraph)
{

	struct timeval t1, t2, t3;
	gettimeofday(&t1, NULL);

	vector<double> fMatrix1(9);
	double* F1 = fMatrix1.data();
	int inliers = computeFmatrix(F1,matchGraph[i][k].second,KeyInfo[i],KeyInfo[k]); 
	if(inliers == -1) { 
		printf("\n   [Triplet Matching] Could Not Find 16 Inliers to computed F, Exiting\n");
		return false;
	}

	vector<double> fMatrix2(9);
	double* F2 = fMatrix2.data();
	inliers = computeFmatrix(F2,matchGraph[j][k].second,KeyInfo[j],KeyInfo[k]); 
	if(inliers == -1) { 
		printf("\n   [Triplet Matching] Could Not Find 16 Inliers to computed F, Exiting\n");
		return false;
	}

  	Gridder rGrid(16, dim[k].first, dim[k].second, numSift[k], KeyInfo[k]);

	vector<pair<int,int> > tMatches;
	vector<pair<int,int> > tMatches1;
	for(int c = 0 ;c < matchGraph[i][j].first; c++)
	{
		int sid1,sid2;
		sid1 = matchGraph[i][j].second[c].first;
		sid2 = matchGraph[i][j].second[c].second;
		float x1,y1,x2,y2,x3,y3;
		x1 = KeyInfo[i][sid1].x;
		y1 = KeyInfo[i][sid1].y;
		x2 = KeyInfo[j][sid2].x;
		y2 = KeyInfo[j][sid2].y;

		float x1n,y1n,x2n,y2n,x3n,y3n;

		x1n = x1 - (dim[i].first/2.0) ;
		x2n = x2 - (dim[j].first/2.0) ;
		y1n = (dim[i].second/2.0) - y1;
		y2n = (dim[j].second/2.0) - y2;

		vector<float> l1(3,0);
		vector<float> l2(3,0);

		l1[0] = F1[0]*x1 + F1[1]*y1 + F1[2];
		l1[1] = F1[3]*x1 + F1[4]*y1 + F1[5];
		l1[2] = F1[6]*x1 + F1[7]*y1 + F1[8];

		l2[0] = F2[0]*x2 + F2[1]*y2 + F2[2];
		l2[1] = F2[3]*x2 + F2[4]*y2 + F2[5];
		l2[2] = F2[6]*x2 + F2[7]*y2 + F2[8];

		Point3f p1 = cv::Point3f(l1[0],l1[1],l1[2]);
		Point3f p2 = cv::Point3f(l2[0],l2[1],l2[2]);
		Point3f p3 = p1.cross(p2);

		x3 = p3.x/p3.z;
		y3 = p3.y/p3.z;

//		x3 = x3n + (dim[k].first/2.0) ; 
//		y3 = (dim[k].second/2.0) - y3n ;

		map<int,int> nearbySift;
		rGrid.getGridPoints(x3,y3,nearbySift);

		int sid3 = -1;
		int minDis = 999999;
		for(auto& nit: nearbySift)
		{
			int sid3c = nit.first;
			int dis1 = 0,dis2 = 0;
			for(int cntr = 0 ; cntr < 128; cntr++)
			{
				dis1 = dis1+ (desc[i][sid1*128 + cntr]-desc[k][sid3c*128 + cntr])*(desc[i][sid1*128 + cntr]-desc[k][sid3c*128 + cntr]) ;
			}
			if(dis1 < minDis)
			{
				minDis = dis1;
				sid3 = sid3c;
			}
		}

		if(sid3 != -1)
		{
			int dis2 = 0;
			for(int cntr = 0 ; cntr < 128; cntr++)
			{
				dis2 = dis2+ (desc[j][sid2*128 + cntr]-desc[k][sid3*128 + cntr])*(desc[j][sid2*128 + cntr]-desc[k][sid3*128 + cntr]) ;
			}
			if((float)minDis/dis2 >= 0.8 && (float)minDis/dis2 <= 1.25)
			{
				tMatches.push_back(make_pair(sid1,sid3));
				tMatches1.push_back(make_pair(sid2,sid3));
			}
		}
							

//		visualizeDBG((char *)imagePath[i].c_str(),(char *)imagePath[j].c_str(),(char *)imagePath[k].c_str(),l1,l2,x1,y1,x2,y2,x3,y3);
	}

	if(!tMatches.empty())
	{
//		visualizeTMatches((char *)imagePath[i].c_str(),(char *)imagePath[k].c_str(),i,k,KeyInfo[i], KeyInfo[k], tMatches); 
		writetMatchesToFile(i,k,tMatches, "../results/matchGraph.txt",matchGraph); 
	}
	if(!tMatches1.empty())
	{
//		visualizeTMatches((char *)imagePath[j].c_str(),(char *)imagePath[k].c_str(),j,k,KeyInfo[j], KeyInfo[k], tMatches1); 
		writetMatchesToFile(j,k,tMatches1, "../results/matchGraph.txt",matchGraph); 
	}
	cout << endl ;
	cout << "Triplet Matching Done\n" ;
	cout << "   Image " << i << " : " << numSift[i] << " features" << endl;  
	cout << "   Image " << j << " : " << numSift[j] << " features" << endl;  
	cout << "   Image " << k << " : " << numSift[k] << " features" << endl;  
	cout << "   Matches transfered  : " << matchGraph[i][k].first << endl;

	return true;

}



int main(int argc,char *argv[])
{
	if(argc != 4)
	{
		printf("To run the program, type ./a.out intput_images_list keys_list dims_list\n");
		return 0;
	}

	struct timespec t1, t2, t3, t4, t5;



	// Read all image names and corresponding key file names
	std::vector<std::string> listImg;
	std::ifstream imgFiles;
	std::string line;
	imgFiles.open(argv[1]);

	while(!imgFiles.eof())
	{
		imgFiles >> line;
		listImg.push_back(line);
	}

	imgFiles.close();

	std::vector<std::string> listKey;
	std::ifstream keyFiles;
	keyFiles.open(argv[2]);

	while(!keyFiles.eof())
	{
		keyFiles >> line;
		listKey.push_back(line);
	}

	keyFiles.close();

	listKey.resize(listKey.size()-1);
	listImg.resize(listImg.size()-1);
	// Load all the features into the main memory

	vector<pair<int,int> > dims;
	std::ifstream dimsFiles;
	int h,w;
	dimsFiles.open(argv[3]);

	while(!dimsFiles.eof())
	{
		dimsFiles >> w >> h;
		dims.push_back(make_pair(w,h));
	}

	dimsFiles.close();

	std::vector<unsigned char*> allDescriptors(listKey.size());
	std::vector<keypt_t* > allKeys(listKey.size());
	vector<int> numSift(listKey.size(),0);


	std::cout << listKey.size() << std::endl;

	for(int i = 0 ; i < listKey.size(); i++)
	{
		std::cout << listKey[i] << "\n" ; 
		numSift[i] = ReadKeyFile((char *)listKey[i].c_str(),&allDescriptors[i], &allKeys[i]);

		printf("reading id %d\n",i);
	}

	cout << "All sift reading complete\n" ;

	vector<vector<pair<int,vector<pair<int,int> > > > > matchGraph(listKey.size(), vector<pair<int,vector<pair<int,int> > > > (1));
	
	cout << "Initializing match graph...\n" ;

	clock_gettime(CLOCK_MONOTONIC, &t1);
	
	initializeMatchGraph(allKeys.size(),matchGraph);

	cout << "MatchGraph initialization complete\n" ;
	cout << "Computing initial matchgraph...\n" ;


	
	int n = allKeys.size();



	for(int i = 0 ; i < n; i++)
	{
		for(int j = i+1 ; j < n; j++)
		{
			cout << "20% Matching between " << i << " " << j << endl;
			bool status;
			status = matchImagePair20(i,j,listImg[i],listImg[j],dims[i].first,dims[i].second,dims[j].first,dims[j].second,numSift[i],numSift[j],allKeys[i],allKeys[j],allDescriptors[i],allDescriptors[j],matchGraph);
		}
	}

	clock_gettime(CLOCK_MONOTONIC, &t2);
	double time1 = ((t2.tv_sec - t1.tv_sec)*1000) + (((double)(t2.tv_nsec - t1.tv_nsec))/1000000.0);
	
	cout << "Match Graph construction complete\n" ;
	cout << "Time taken : " << time1 << endl;


//	cout << "Reading initial matchgraph...\n" ;

//	readInitialMatchGraph(argv[4], matchGraph);

//	cout << "Reading initial match graph complete\n" ;
	
	
	clock_gettime(CLOCK_MONOTONIC, &t1);

	vector< vector <int> > freqTable(n,vector<int> (n,0));
	// 0 : invalid , 1 : valid
	vector< vector< vector< bool> > > triplets(n, vector< vector< bool> > (n, vector<bool> (n,0))) ;

	int thres = 100;
	
	
	for(int i = 0 ; i < allKeys.size(); i++)	// for all triplets
	{
		for(int j = i+1 ; j < allKeys.size(); j++)
		{
			for(int k = j+1 ; k < allKeys.size(); k++)
			{
				if(matchGraph[i][j].first > thres && matchGraph[j][k].first > thres) // valid T
				{
					cout << "Valid T found between " << i << " " << j << " " << k << endl;
					freqTable[i][j]++;
					freqTable[i][k]++;
					freqTable[j][k]++;
					triplets[i][j][k] = 1;
					// insert i j k in triplet

				}
			}
		}

	}

	clock_gettime(CLOCK_MONOTONIC, &t2);
	double time2 = ((t2.tv_sec - t1.tv_sec)*1000) + (((double)(t2.tv_nsec - t1.tv_nsec))/1000000.0);

	cout << "Frequency table created\n" ;
	
	clock_gettime(CLOCK_MONOTONIC, &t1);
	
	vector <pair < pair<int,int>,int> > anotherFreqTable;

	for(int i = 0 ; i < n; i++)
	{
		for(int j = 0 ; j < n ; j++)
		{
			if(freqTable[i][j]!=0)
			{
				anotherFreqTable.push_back(make_pair(make_pair(i,j),freqTable[i][j]));
	//			cout << i << " " << j << " " << freqTable[i][j] << endl;
			}
		}

	}
	cout << "Sorting the frequency table\n" ;

	struct {
		bool operator()(pair<pair<int,int>,int> a, pair<pair<int,int>,int> b)
		{   
			return a.second < b.second;
		}   
	} customLess;


	sort(anotherFreqTable.begin(),anotherFreqTable.end(),customLess);

	stack <pair < pair<int,int>,int> > sortedFreqTable;
	for (pair<pair<int,int>,int> a : anotherFreqTable) {
		sortedFreqTable.push(a);
	}   


	cout << "Frequency table sorted\n" ;

	vector<pair<int,int> > fullMatches;
	
	//  0 - unmatched , 1 - fully matched
	vector<vector<int> > pairMatched(n,vector<int> (n,0)) ;

	/*
	for(int i = 0 ; i < n ; i++)
	{
		for(int j = 0 j < n ; j++)
		{
			for(int k = 0 ; k < n; k++)

			*/

	FILE *mf = fopen("../results/matchGraph.txt","w");
	fclose(mf);

	clock_gettime(CLOCK_MONOTONIC, &t2);
	double time3 = ((t2.tv_sec - t1.tv_sec)*1000) + (((double)(t2.tv_nsec - t1.tv_nsec))/1000000.0);

	
	while(!sortedFreqTable.empty())
	{
		pair<pair<int,int>,int> edgeNfreq = sortedFreqTable.top();
		sortedFreqTable.pop();
		int i,j;
		i = edgeNfreq.first.first ;
		j = edgeNfreq.first.second;
		if(pairMatched[i][j] == 0)
		{
			cout << "Full Matching between " << i << " " << j << endl;
			bool status;
//			status = doFullMatching(i,j,listImg[i],listImg[j],fullMatches,matchGraph);
			status = matchImagePair(i,j,listImg[i],listImg[j],dims[i].first,dims[i].second,dims[j].first,dims[j].second,numSift[i],numSift[j],allKeys[i],allKeys[j],allDescriptors[i],allDescriptors[j],matchGraph);
			if(status)
			{
				pairMatched[i][j] = 1;
				cout << "Full Matching done \n";
				for(int k = 0 ; k < n ; k++)
				{
					if(triplets[i][j][k] == 1 && pairMatched[i][k] == 0) // valid n unmatched
					{
						// do triplet matching
						cout << "   triplet matching between " << i << " " << j << " " << k << endl;
						tripletTranfer(i,j,k,listImg,dims,numSift,allKeys,allDescriptors,matchGraph);
						//						status = transferMatches(i,j,k,listImg,listKey,matchGraph,allKeys[i],allKeys[j],allKeys[k]);
//						if(status == 0)
							pairMatched[i][k] = 1;
					}
				}
			}
		}

		/*
		   for any unmatched triplet, 
		   if 2 edges are matched, 
		   do point transfer
		   mark third edge as matched
		 */
	}

	cout << "Total time : " << time1 + time2 + time3 << " sec\n" ;
	
	cout << "Match Graph construction complete\n" ;

	cout << "Left over pairs : \n" ;
	for(int i = 0 ; i < n ; i++)
	{
		for(int j = i+1 ; j < n; j++)
		{
			if(pairMatched[i][j] == 0 && i!=j && matchGraph[i][j].first != 0)
			{
				int status = matchImagePair(i,j,listImg[i],listImg[j],dims[i].first,dims[i].second,dims[j].first,dims[j].second,numSift[i],numSift[j],allKeys[i],allKeys[j],allDescriptors[i],allDescriptors[j],matchGraph);
				cout << i << " " << j << endl ;
			}
		}
	}
	return 0;
}
